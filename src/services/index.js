import axios from 'axios';

const RootPath = 'http://localhost:3009/';

const Get = (path) =>  {
    const promise = new Promise((resolve, reject) =>{
        const GetAPIURL = RootPath.concat(path);
        axios.get(GetAPIURL)
            .then(res => {
                resolve(res.data);
            },err => {
                reject(err);
            })
    })
    return promise;
}

const Del = (deleteID) => {
    const promise = new Promise ((resolve,reject)=>{
        const GetAPIURL = RootPath.concat(deleteID);
        axios.delete(GetAPIURL)
        .then(res=>{
            resolve(res.data);
        },err =>{
            reject(err);
        })
    })
    return promise;
}

const Post = (path , data) =>{
    const promise = new Promise ((resolve,reject)=>{
        const GetAPIURL = RootPath.concat(path);
        axios.post(GetAPIURL , data)
        .then(res=>{
            resolve(res.data);
        },err =>{
            reject(err);
        })
    })
    return promise;
}
const Put = (path , data) =>{
    const promise = new Promise ((resolve, reject)=>{
        const GetAPIURL = RootPath.concat(path);
        axios.put(GetAPIURL , data)
        .then(res =>{
            resolve(res.data);
        },err =>{
            reject(err);
        })
    })
    return promise
}

const getCorporateName =() => Get('corporate');
const getContact = ()=> Get('contact');
const getSearching = (e)=> Get(e);

const API ={
    getCorporateName,
    getContact,
    getSearching,
    Del,
    Post,
    Put
}

export default API;
