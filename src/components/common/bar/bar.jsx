import React from 'react';
import {
    BarChart,Line,Bar, XAxis, YAxis, CartesianGrid, Tooltip,
  } from 'recharts';

const Bars = props => {
    return ( 
    <div className="m-3 col-10 " style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
        <div className="m-2" style={{overflow:"auto"}}>
            <h6>{props.bar.title}</h6>
             <BarChart
                width={700}
                height={300}
                data={props.bar.data}
                margin={{
                top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Bar dataKey="total" fill="#89c531" />
                <Line type="monotone" dataKey="total" stroke="#ff7300" />
            </BarChart>
            <button onClick={()=> props.onClickMonth(props.bar.id)} id={props.bar.id} className="btn btn-info btn-sm m-2" style={{width: "100px", marginRight:"10px" }}
                >Month 
            </button>
            <button onClick={()=> props.onClickYear(props.bar.id)} id={props.bar.id} className="btn btn-info btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                >Year
            </button>
        </div>
    </div> 
    );
};
 
export default Bars;