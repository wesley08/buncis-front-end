import React, { Component } from "react";
import "./navbar.css";


class Navbar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bgcolorr ">
        <button onClick={this.props.onToggle}
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarTogglerDemo01"
          aria-controls="navbarTogglerDemo01"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
         <i className="fa fa-bars" />
        </button>
        <div className="collapse navbar-collapse navbardif" id="navbarTogglerDemo01">
                <button 
                  onClick={this.props.onToggle}
                  className="btn btn-outline-info my-2 my-sm-0 btnborder"
                >
                   <i className="fa fa-bars" />
                </button>
        </div>
      </nav>
    );
  }
}

export default Navbar;
