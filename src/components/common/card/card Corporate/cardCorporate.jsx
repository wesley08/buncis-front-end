import React from 'react';
import "./cardCorporate.css";
import { Link } from 'react-router-dom';


const cardCorporate = props => {
    return ( 
            <div className="p-1 col-xl-3 col-12 col-sm-6 col-md-6 col-lg-4 ubahColPadding">
                <div className="box box-widget widget-user-2">
                    <div className="widget-user-header ">
                        <div className="widget-user-image">
                            <img style={{height:"50px", width :"50px"}}src={require ('./../../../../assets/' +props.card.image)} alt=""/>
                        </div>
                        <h4 className="widget-user-username">{props.card.title}</h4>
                        <p className="widget-user-desc">{props.card.address}</p>
                    </div>
                    <div className="box-footer ">
                        <ul className="nav nav-stacked" style={{display:"inline"}}>
                            <li><a >Contact <span className="pull-right badge bg-blue">{props.card.contacttotal}</span></a></li>
                            <li><a >Collaboration <span className="pull-right badge bg-aqua">{props.card.collaborationtotal}</span></a></li>
                            <li>
                                <Link to="/corporate/corporateAddEdit" >
                                    <button onClick={()=> props.update(props.card.id)} type="button" className="btn btn-primary btn-size btn-block" style={{borderRadius:0}}>
                                        Edit Information
                                    </button>
                                </Link>
                            </li>
                            <li> <button onClick={()=> props.delete(props.card.id)}type="button" className="btn btn-danger btn-size btn-block" style={{borderRadius:0}}>Delete all data</button></li>                 
                        </ul>
                    </div>
                </div>
            </div>
            
     );
}
 
export default cardCorporate;