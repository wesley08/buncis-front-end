import React from 'react';

const Button = (props) => {
    return ( 
        <button type="button" onClick={()=>props.buttonclick()} className={"btn btn-"+ props.button.tema + props.button.btnSize + props.button.margin}>{props.button.label}</button>
     );
}
 
export default Button;