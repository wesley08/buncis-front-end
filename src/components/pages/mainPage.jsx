import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Navbar from '../common/navbar/navbar';
import Sidebar from '../common/sidebar/sidebar';
import Dashboard from './dashboard'
import CollaborationBar from './collaborationBar';
import Corporate from '../pages/corporate/corporate/corporate';
import "../../assets/Style/_all-skins.min.css";
import "../../assets/Style/styles.css"
import CorporateAddExit from './corporate/corporate/corporateAddEdit';
import "../../assets/Style/AdminLTE.min.css";
import CorporateContact from '../pages/corporate/corporateContact/corporateContact';
import CorporateContactAddEdit from './corporate/corporateContact/corporateContactAddEdit';
import Collaborationn from '../pages/collaboration/collaboration/collaboration';
import CollaborationType from '../pages/collaboration/collaboration Type/collaborationType';
import CollaborationAddEdit from '../pages/collaboration/collaboration/collaborationAddEdit';
import CollaborationTypeAddEdit from '../pages/collaboration/collaboration Type/collaborationTypeAddEdit';

class MainPage extends Component {
    state = { 
        status: {
            sidebar: "block",
            content: "col-10",
        }, 
        sidebars: [
            {
              id: 1,
              name: "Dashboard",
              icon: "dashboard",
              nameClass: "dasboard",
              link:"/"
            },
            {
              id: 2,
              name: "Corporate",
              icon: "building",
              nameClass: "corporate",
              link:"/corporate"
            },
            {
              id: 3,
              name: "Corporate Contact",
              icon: "address-book",
              nameClass: "corporateContact",
              link:"/corporateContact"

            },
            {
              id: 4,
              name: "Collaboration",
              icon: "user-plus",
              nameClass: "collaboration",
              link:"/collaboration"

            },
            {
              id: 5,
              name: "Collaboration Type",
              icon: "users",
              nameClass: "collaborationType",
              link:"/collaborationType"

            },
            {
              id: 6,
              name: "Collaboration Bar",
              icon: "signal",
              nameClass: "collaboration",
              link:"/collaborationBar"

            }
        ]
    }
    constructor() {
        super();
        this.toggleSidebar = this.toggleSidebar.bind(this);
      }
    
      toggleSidebar() {
        const status = {
          sidebar: this.state.status.sidebar === "block" ? "none" : "block",
          content: this.state.status.content === "col-10" ? "col-12" : "col-10"
          
        };
        this.setState({ status});
      }
    render() { 
        return (
            <div id="wrapper" className="row modifrow">
                <Sidebar
                    status={this.state.status.sidebar}
                    sidebars={this.state.sidebars}
                />
                <div className={this.state.status.content +  " bgcolor"}>
                 <Navbar onToggle={this.toggleSidebar} />
                   <div className="container-fluid">
                        <div className="mt-4">
                            <Switch>
                                <Route path="/collaborationType" component={CollaborationType}/>
                                <Route path="/collaborationTypeAddEdit" component={CollaborationTypeAddEdit}/>
                                <Route path="/collaboration/collaborationAddEdit" component={CollaborationAddEdit}/>
                                <Route path="/collaboration" component={Collaborationn}/>
                                <Route path="/collaborationBar" component={CollaborationBar}/>
                                <Route path="/corporate/corporateAddEdit" component={CorporateAddExit}/>
                                <Route path="/corporate/corporateContactAddEdit" component={CorporateContactAddEdit}/>
                                <Route path="/corporateContact" component={CorporateContact}/>
                                <Route path="/corporate" component={Corporate}/>
                                <Route path="/"  component={Dashboard}/>
                            </Switch>
                        </div>
                    </div>    
                </div>
            </div>
        );
    }
}
 
export default MainPage;