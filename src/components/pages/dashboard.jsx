import React, { Component } from 'react';
import Table from '../common/table/table/table';
import Card from '../common/card/card Dashboard/card'
import {connect} from 'react-redux'
import API from '../../services/index';
import axios from 'axios'

class Dashboard extends Component {
    state = {
        table : {
            tableHead: [
             {
                 id:"a1",
                 path:"date",
                 label : "Date"
             },
             {
                 id:"a2",
                 path:"detail",
                 label :"Detail"
             },
             {
                 id:"a3",
                 path:"type",
                 label : "Type"
             },
             {
                 id:"a4",
                 path:"contactPerson",
                 label : "Contact Person"
             },
             {
                 id:"a5",
                 content: user => (
                     <div>
                         <button className="btn btn-success btn-sm"  onClick={this.handleClick} style={{ marginRight:"10px" }}
                         >Views Detail
                         </button>
                         <button className="btn btn-success btn-sm" onClick={this.handleClick2} style={{ marginRight:"10px" }}
                         >Contact Person Detail
                         </button>
                     </div>
                 )
             },
            ],
         tableBody :[],colors: "bg-success"
        },corporate:[]
     }
    componentWillMount(){
        this.props.change_totalContact(0 , 50)
        this.setState(this.state.table.tableBody = this.props.tableBody)  
        } 
        
    render() { 
        return (  
            <div>
                <h2 className="dashboardstyle">Dashboard</h2>
                <div className=" card-deck mt-3 mb-3">
                    {this.props.cards.map(card => (
                        <Card key={card.id} card={card} />
                    ))}
                </div>
                <div style={{overflow:"auto"}}>
                    <Table users={this.state.table} />
                </div> 
            </div>
        );
    }
}

const mapStateToProps= (state) => {
    return{
        cards : state.dashboard.cards,
        tableBody : state.dashboard.tableBody,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        // getAPIBUNCIS : ()=> dispatch(API.GetAPIBUNCIS('get-info-corporate')),
       change_totalContact: (totalContact , totalCollaboration)=> dispatch({type : "CHANGE_TOTAL", totalContact : totalContact , totalCollaboration : totalCollaboration})
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);