import React, { Component } from 'react';
import CardCorporate from '../../../common/card/card Corporate/cardCorporate';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchCorporate, deleteCorporate, getID, UPDATE_CORPORATE } from '../../../../redux/action/actionCorporate';


class Corporate extends Component {
    componentWillMount(){
       this.props.getapi()
      }
    render() { 
        return ( 
         <div>
             <div style={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
                <h2 >Corporate</h2> 
                <Link className="designLink" to="/corporate/corporateAddEdit">
                    <button className="btn btn-info btn-sm m-2" style={{  marginRight:"10px" }}
                        >Add New Corporate
                    </button>
                </Link>  
            </div>
            <div className="card-deck">
                <div className="row m-2 row " style={{overflow:"auto",height:"600px"}}> 
                    {this.props.corporate.map(card => (
                        <CardCorporate key={card.id} card={card} update={this.props.getID} delete={this.props.deleteCorporate}/>
                    ))}
                </div>
            </div>
        </div> );
    }
}
 const mapStateToProps = (state)=>{
    return {
        isUpdate : state.corporate.isUpdate,
        corporate: state.corporate.corporate,
        loading: state.corporate.loading,
        error: state.corporate.error
    }
 }

 const mapDispatchToProps = (dispatch) =>{
     return{
        getapi : ()=> dispatch(fetchCorporate()),
        deleteCorporate : (e) => dispatch(deleteCorporate(e)),
        getID : (e) => dispatch({type:UPDATE_CORPORATE},getID(e)),
     }
 }
export default connect(mapStateToProps,mapDispatchToProps)(Corporate);