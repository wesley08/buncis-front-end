import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { postCorporate, putCorporate, DEFAULT_UPDATE_CORPORATE } from '../../../../redux/action/actionCorporate';

class CorporateAddExit extends Component {
    state ={
        form:{
            id:0,
            address: "",
            title: "",
            image: "tel1.png",
            contacttotal: 1,
            collaborationtotal: 2
        }
    }
    handleSubmit= ()=>{
        if(this.props.isUpdate){
          this.props.putCorporate(this.state.form);
          this.props.setUpdateDefault()
        }else{
           this.props.postCorporate(this.state.form);
        }
    }
    handleChange =(event)=>{
        let form = {...this.state.form};
        form[event.target.name] =event.target.value;
        this.setState({
          form
        })
    }
    render() {
        return ( 
            <div>
                <div style={{display:"flex", justifyContent:"space-between"}}>
                    <div style={{display:"flex"}}>
                        <h1>
                            Corporate <sub className="ubahColPadding">[Add New] or [Edit]</sub>
                        </h1>  
                    </div>
                    <div>
                        <Link className="designLink" to="/corporate">
                            <button  className="btn btn-danger btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                                >Cancel
                            </button>
                        </Link>
                        
                        <Link className="designLink" to="/corporate/">
                        <button  className="btn btn-success btn-sm m-2" onClick={this.handleSubmit} style={{ width: "100px", marginRight:"10px" }}
                            >Save
                        </button>
                        </Link>
                    </div>
                </div>
                <div className="row">
                    <div className="m-3 col-xl-5 col-10 col-sm-10 col-md-10 col-lg-5 m-3 p-3" style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
                        <h6 className="m-1">Picture</h6>
                        <div className="custom-file">
                            <input type="file" className="custom-file-input" id="customFile"/>
                            <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                        </div>
                    </div>
                    <div className="m-3 col-xl-5 col-10 col-sm-10 col-md-10 col-lg-5 m-3 p-3" style={{backgroundColor:"white", width:"48%", borderTop:"#17a2b8 3px solid"}}>
                        <h6 className="m-1">Information</h6>
                        <hr/>
                        <form className="m-3" action="">
                            <h6>Corporeate Name</h6>
                            <input className="p-1 form-control" name="title" onChange={this.handleChange}type="textfield" style={{width:"100%"}} placeholder="Corporate Name"/>
                            <h6 className="mt-2">Address</h6>
                            <textarea className="p-1 form-control" name="address"onChange={this.handleChange}type="area" style={{width:"100%"}} placeholder="Enter Address"/>
                        </form>
                    </div>
                </div>       
            </div>   
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        isUpdate : state.corporate.isUpdate
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        postCorporate : (e) => dispatch(postCorporate(e)),
        putCorporate : (e) => dispatch(putCorporate( e)),
        setUpdateDefault : () => dispatch({type:DEFAULT_UPDATE_CORPORATE})
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(CorporateAddExit);