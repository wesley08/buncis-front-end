import React, { Component } from 'react';
import Table from '../../../common/table/table/table';
import { Link } from 'react-router-dom';
import API from '../../../../services/index'


class CorporateContact extends Component {
    state = { 
        valueSelect : "Telkom",
        table : {
        tableHead: [
         {
             id:"1",
             path:"name",
             label : "Name"
         },
         {
             id:"2",
             path:"company.name",
             label : "Coporate Name"
         },
         {
             id:"3",
             path:"position",
             label :"Position"
         },
         {
             id:"4",
             path:"email",
             label : "Email"
         },
         {
             id:"5",
             path:"phone",
             label : "Phone"
         },
         {
             id:"7",
             content: user => (
                 <div>
                     <button className="btn btn-warning btn-sm mr-1"  onClick={()=>this.handleEdit(user)} 
                     >Edit
                     </button>
                     <button className="btn btn-danger btn-sm mr-1" onClick={this.deleteCorporate} 
                     >Delete
                     </button>
                 </div>
             )
         },
        ],
     tableBody :[] 
     ,colors: "bg-success"},corporateName :[]
    }
    componentDidMount(){
        this.getAPICorporate() 
        this.getSearching(this.state.valueSelect)
      }

    getAPICorporate = ()=>{
        API.getCorporateName().then(res => {
            this.setState({corporateName : res})
        })
    }
    getSearching = (getValue) =>{
        API.getSearching(`contact?company.name=${getValue}`).then(res =>{
            this.setState(this.state.table.tableBody = res)
        })
    }
    handleEvent =(event)=>{
        this.getSearching(event.target.value)
    }
    handleEdit = (event)=>{
       console.log(event)
    }
    render() { 
        return ( 
            <div >
              <div style={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
                <h2 >Corporate Contact
                </h2> 
                <Link className="designLink" to="/corporate/corporateContactAddEdit">
                    <button  className="btn btn-info btn-sm m-2" style={{  marginRight:"10px" }}
                        >Add New Contact
                    </button>
                </Link> 
              </div>
              <div className="row">
                <div className=" col-xl-4 col-10 col-sm-10 col-md-6 col-lg-4 m-3" style={{backgroundColor:"white", width:"35%", borderTop:"#17a2b8 3px solid"}}>
                  <div className="m-3" style={{ display:"flex",justifyContent:"space-between"}}>
                    Filters  
                    <button  className="btn btn-info btn-sm "
                            >Apply Filters
                    </button>
                  </div>
                  <hr/> 
                  <div className="m-2">
                    <form className="m-3"  action="">
                      <h6 className="mb-3">Name</h6>
                      <input className="mb-3 form-control" type="text" style={{width:"100%"}} placeholder="Enter the Name"/>
                      <h6 className="mb-3">Corporate Name</h6>
                      <select onChange={this.handleEvent}  className="mb-3 custom-select custom-select-sm"style={{width:"100%"}}>
                        { this.state.corporateName.map(corporate => 
                            <option value={corporate.title}key={corporate.id}>
                                {corporate.title}
                            </option>)}
                      </select>
                      <h6 className="mb-3">Contact Person of Collaboration</h6>
                      <select  className=" custom-select custom-select-sm mb-3"style={{width:"100%"}}>
                        {<option></option>}
                      </select>
                    </form>
                  </div>   
                </div>
                <div className="col-xl-7 col-10 col-sm-10 col-md-10 col-lg-7 m-3" style={{backgroundColor:"white", width:"60%", borderTop:"#17a2b8 3px solid"}}>
                  <h6 className="m-3">People Details</h6> 
                  <hr/>    
                  <div className=" ubahColPadding"style={{overflow:"auto"}}>
                    <Table users={this.state.table}/>
                  </div>
                </div>  
              </div>
            </div>
         );
    }
}
 
export default CorporateContact;