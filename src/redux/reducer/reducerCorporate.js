
import {
    FETCH_CORPORATE_BEGIN,
    FETCH_CORPORATE_SUCCESS,
    FETCH_CORPORATE_FAILURE,
    UPDATE_CORPORATE,
    DEFAULT_UPDATE_CORPORATE
  } from '../action/actionCorporate';
const corporateState = {
    corporate: [],
    loading: false,
    error: null,
    isUpdate : false
}
     
const reducerCorporate = (state = corporateState ,action) => {
    switch(action.type) {
        case FETCH_CORPORATE_BEGIN:
          return {
            ...state,
            loading: true,
            error: null
          };
    
        case FETCH_CORPORATE_SUCCESS:
          return {
            ...state,
            loading: false,
            corporate: action.payload
          };
    
        case FETCH_CORPORATE_FAILURE:
          return {
            ...state,
            loading: false,
            error: action.payload.error,
            corporate: []
          };
        case UPDATE_CORPORATE:
        return{
          ...state,
          isUpdate : true,
        };
        case DEFAULT_UPDATE_CORPORATE:
        return {
          ...state, 
          isUpdate: false
        };
        default:
          return state;
      }
}

export default reducerCorporate;