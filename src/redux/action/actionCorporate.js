import API from '../../services/index';
export const FETCH_CORPORATE_BEGIN   = 'FETCH_CORPORATE_BEGIN';
export const FETCH_CORPORATE_SUCCESS = 'FETCH_CORPORATE_SUCCESS';
export const FETCH_CORPORATE_FAILURE = 'FETCH_CORPORATE_FAILURE';
export const DELETE_CORPORATE = 'DELETE_CORPORATE';
export const UPDATE_CORPORATE = 'UPDATE_CORPORATE';
export const DEFAULT_UPDATE_CORPORATE = 'DEFAULT_UPDATE_CORPORATE'


//action
export const fetchCorporateBegin = () => ({
  type: FETCH_CORPORATE_BEGIN
});

export const fetchCorporateSuccess = corporate => ({
  type: FETCH_CORPORATE_SUCCESS,
  payload:corporate
});

export const fetchCorporateFailure = error => ({
  type: FETCH_CORPORATE_FAILURE,
  payload: { error }
});

export const isUpdate = () => ({
  type: UPDATE_CORPORATE,
});

export const setUpdateDefault =()=>({
  type : DEFAULT_UPDATE_CORPORATE
})

export let ID = 0

export function getID (e){
    ID = e 
    isUpdate()
  }


  export function fetchCorporate() {
    return dispatch => {
      dispatch(fetchCorporateBegin());
      return API.getCorporateName()
      .then(res =>{
        dispatch(fetchCorporateSuccess(res));
      })
      .catch(error => dispatch(fetchCorporateFailure(error)));
    }
    };
  
  export function deleteCorporate(e){
    return dispatch =>{
      return API.Del(`corporate/${e}`)
      .then(res=>{
        dispatch(fetchCorporate());
      })
    }
  }

  export function postCorporate(e){
    return dispatch =>{
      return API.Post('corporate/', e)
      .then(res=>{
        dispatch(fetchCorporate());
      })
    }
  }

  export function putCorporate(e){
    return dispatch =>{
      return API.Put(`corporate/`+ ID ,e)
      .then(res=>{
        dispatch(fetchCorporate());
      })
    }
  }

