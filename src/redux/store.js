import { createStore, combineReducers,applyMiddleware } from "redux";
import reducerDashboard from './reducer/reducerDashboard';
import thunk from "redux-thunk";
import reducerCorporate from './reducer/reducerCorporate';

const rootReducer = combineReducers({
    corporate :reducerCorporate,
    dashboard :reducerDashboard
  })

const store = createStore( rootReducer,applyMiddleware(thunk) );

export default store;